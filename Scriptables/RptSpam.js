// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// always-run-in-app: true; icon-color: yellow;
// icon-glyph: magic; share-sheet-inputs: url, plain-text;

params = {
  spamNum: args.shortcutParameter.spamNumbers,
  spamBody: args.shortcutParameter.spamBody,
  args: args.plainTexts, 
  arg: args.shortcutParameter,
  results: {}
}

async function addPhoneToSpam(curContact, spamNum)
{
  let phones = curContact.phoneNumbers
  phones.push({value: spamNum})
  curContact.phoneNumbers = phones
  params.results.spamContactPhones = phones
  
  Contact.update(curContact)
  await Contact.persistChanges()

  params.results.spamContactID = curContact.identifier
}

async function reportSpamTextToAtt(spamNum, spamBody, attSpamRptNums = ['7726'])
{
  //Need to send 2 messages - first with the spam body, second with #
  let bodyMsg = new Message()
  bodyMsg.body = spamBody
  bodyMsg.recipients = attSpamRptNums
  let _ = await bodyMsg.send()

  let numMsg = new Message()
  numMsg.body = spamNum
  numMsg.recipients = attSpamRptNums
  _ = await numMsg.send()

  params.results.reported = true
}

defContCont = await ContactsContainer.default()
allContacts = await Contact.all([defContCont])
for(var curContact of allContacts)
{
  if(curContact.organizationName === "Spam Phones")
  {
    await addPhoneToSpam(curContact, params.spamNum)
    break
  }
}

//await reportSpamTextToAtt(params.spamNum, params.spamBody)
Script.setShortcutOutput(JSON.stringify(params, undefined, 2))
Script.complete()
