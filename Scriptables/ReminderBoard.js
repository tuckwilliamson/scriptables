// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: green; icon-glyph: magic;
let size = 35
let offset = 2

function getFirstCharOfWords(word) {
  if (word && word.length) {
    return word[0].toUpperCase()
  } else {
    return ""
  }
}

function countOfChar(str, char) {
  return (str.match(new RegExp(char,'g'))||[]).length
}

function liFromRmdr(rmdr){
  preProcessLi(false, rmdr)
  let cls = []
  if(rmdr.priority){
      cls.push("pri" + rmdr.priority.toString())
  }
  if(rmdr.isCompleted){
    cls.push("checked")
  }
  if(rmdr.notes) cls.push("notes")
  
  if (rmdr.title.trim().startsWith('?')) {
    // Change but don't save. 
    //That way phone still has correct title. 
    let title = rmdr.title.trim()
    cls.push("hiddenTitle")
    
    // Pull just the first letter of each word. 
    title = title.slice(1).trim().split(" ").map(getFirstCharOfWords)
    rmdr.title = title.join("")
  }

  let classes = cls.length ? ` class="${cls.join(" ")}"` : ""

  return `<li${classes}>${rmdr.title}</li>\n`
}

var priMap = [0,9,5,1]
function preProcessLi(allowModSave,rmdr) {
  let dirty = false
  
  if (rmdr.title && rmdr.title.trim().startsWith('!')) {
    let pri = rmdr.title.trim().match(/^\!+/g)[0].length
    
    rmdr.title = rmdr.title.slice(pri).trimStart()
    pri = pri >= 0 && pri <= 3 ? pri : 3
    rmdr.priority = priMap[pri]
    
    dirty = true
  }
  if (allowModSave && dirty) {
    rmdr.save()
  }
}

function makeLiS(rmdrs) {
  return rmdrs.map(r => liFromRmdr(r)).join("\n ")
}

async function makeRmdrList(cals) {

  if(cals && cals.hasOwnProperty('length') && cals.length) {
    let title = cals.map(c => c.title).join(" ");

    let comp = await Reminder.completedToday(cals)
    let todo = await Reminder.allIncomplete(cals)

    let total = comp.length + todo.length

    return `<h2 class="rmdrs"><span class="rmdrTitle">${title}</ span> <span class="rmdrTotal">${comp.length} of ${total}</ span></h2>
<ul>
  ${makeLiS(comp)}
  ${makeLiS(todo.filter(r => r.priority ===1))}
  ${makeLiS(todo.filter(r => r.priority ===5))}
  ${makeLiS(todo.filter(r => r.priority ===9))}
  ${makeLiS(todo.filter(r => !r.priority))}
</ul>`
  }
  return ""
}

let dateFmt = new DateFormatter()
dateFmt.useMediumDateStyle()
dateFmt.useShortTimeStyle()

let cals = [ await Calendar.forRemindersByTitle("Drafts") ]

let rtn = `
<html>
<head>
<meta charset="utf-8">
<title>To Do</title>
<style>
body {
  font-size: 200%;
  width: 758;
  height: 1024;
  /*border: 1px solid black;
  border-radius: 15px;*/
}
* {
  width: inherit;
  max-width: 758;
  
}
h1 {
  font-size: 1.5em;
  padding-left: 10;
  margin-bottom: 0;
  padding-bottom: 0;
  color: #fff;
  background-color: #000;
}
ul {
  list-style: none;
  padding-left: 0;
}
.pri1 {
  font-size: 1.2em;
  text-decoration: underline;
  font-weight: bold;
}
.pri5 {
  font-size: 1.1em%;
  font-weight: bold;
}
.pri9 {
  font-size: 1.05em%;
  text-decoration: underline;
}
.rmdrs {
  width: 90%;
  margin: auto;
}
.rmdrTotal {
  width: 50%;
  position: absolute;
  right: 0;
}
.notes:after {
  content: '📋';
}
.date:after {
  content: '🗓';
}
.loc:after {
  content: '🗺';
}
.hiddenTitle:after {
  content: '🔒';
}
.hiddenTitle {
  background-color: #eee;
}
li {
  width: initial;
  position: relative;
  padding-left: 1.5em;  /* space to preserve indentation on wrap */
}
li.checked:before {
  content: '';  /* placeholder for the SVG */
  position: absolute;
  left: 0;  /* place the SVG at the start of the padding */
  margin-top: 2px;
  width: 37px;
  height: 37px;
  background: url("data:image/svg+xml;utf8,<?xml version='1.0' encoding='utf-8'?><svg width='35' height='35' viewBox='0 0 1792 1792' xmlns='http://www.w3.org/2000/svg'><path d='M813 1299l614-614q19-19 19-45t-19-45l-102-102q-19-19-45-19t-45 19l-467 467-211-211q-19-19-45-19t-45 19l-102 102q-19 19-19 45t19 45l358 358q19 19 45 19t45-19zm851-883v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z'/></svg>") no-repeat;
}
li:before {
  content: '';  /* placeholder for the SVG */
  position: absolute;
  left: 0;  /* place the SVG at the start of the padding */
  margin-top: 2px;
  width: 37px;
  height: 37px;
  background: url("data:image/svg+xml;utf8,<?xml version='1.0' encoding='utf-8'?><svg width='35' height='35' viewBox='0 0 1792 1792' xmlns='http://www.w3.org/2000/svg'><path d='M1312 256h-832q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113v-832q0-66-47-113t-113-47zm288 160v832q0 119-84.5 203.5t-203.5 84.5h-832q-119 0-203.5-84.5t-84.5-203.5v-832q0-119 84.5-203.5t203.5-84.5h832q119 0 203.5 84.5t84.5 203.5z'/></svg> ") no-repeat;
}
</style>
</head>
<body>
<h1>${dateFmt.string(new Date())}</h1>
${await makeRmdrList(cals)}
</body>
</html>
`
// fm = FileManager.iCloud()
// fm.

if (config.runsInApp) {
  let wv = new WebView()
  await wv.loadHTML(rtn, "http://google.com")
  await wv.present()
  Pasteboard.copyString(rtn)
}

Script.setShortcutOutput(rtn)
return rtn
Script.complete()
