// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// always-run-in-app: true; icon-color: deep-green;
// icon-glyph: file-import; share-sheet-inputs: plain-text, image, url;
sd = importModule("showdown")
sjcl = importModule("sjcl")

const keyName = "GLKey"
if(!Keychain.contains(keyName))
{
  let alt = new Alert()
  alt.title = "Set encryption key..."
  alt.message = "Please choose an encryption key. choose a complex long key. It will be biometrically retrieved later so you wont need to type it in again."
  alt.addSecureTextField("Enter encryption key", "")
  alt.addSecureTextField("Enter your encryption key again", "")
  alt.addAction("Enter")
  alt.addCancelAction("Cancel")
  do{
    let cancel = await alt.presentAlert()
    if(cancel)
    {
      Script.setShortcutOutput(null)
      Script.complete()
      return
    }
    // setup in case they need to re-enter
    alt.title = "Keys do not match. Re-try."
  }while(alt.textFieldValue(0) !== alt.textFieldValue(1) && !cancel)
  
  Keychain.set(keyName, alt.textFieldValue(0))
}

iCfiles = FileManager.iCloud()

let glDirectory= iCfiles.documentsDirectory() + "/GL"
if(!iCfiles.isDirectory(glDirectory))
{
  iCfiles.createDirectory(glDirectory, true)
}

// var old = ""
// function logger(logStr)
// {
//   const logFile = iCfiles.documentsDirectory() + "/glLog.txt"
//   old += logStr + "\n"
//   iCfiles.writeString(logFile, old)
// }
function hasArgs()
{
  return args.plainTexts.length || args.urls.length;
}


var targetFilePath = null
var doAppend = hasArgs()
var showContents = !hasArgs()


// logger(`${showContents}
//  ${doAppend}
//  ${args.fileURLs}
//  ${args.images}
//  ${args.notification}
//  ${args.plainTexts}
//  ${args.queryParameters}
//  ${args.shortcutParameter}
//  ${args.urls}
//  ${args.widgetParameter}
// `)

async function addList()
{
  let alt = new Alert("test");
  alt.addTextField("Enter list name", "New List");
  alt.addAction("Create new list");
  alt.addCancelAction("Cancel")
  alt.title = "Set new list name."
  alt.message = "Enter the name for your new list."
  prm = alt.presentAlert()
  return prm.then((idx) => { 
    if(idx)
    {
      // cancelled
      log("User cancelled.")
      Script.setShortcutOutput(null)
      Script.complete()
      return
    }
    let name = alt.textFieldValue(0);
    log(name)
    targetFilePath = iCfiles.joinPath(glDirectory, name + ".md")
    iCfiles.writeString(targetFilePath,"")
  })
}

var toDos = []
flist=iCfiles.listContents(glDirectory)

let uiTbl = new UITable()
let addRow = new UITableRow()
let addCell = addRow.addButton("Add List...")
addCell.dismissOnTap = true
addCell.onTap = () => {
  toDos.push(addList)
}

addCell.centerAligned()

uiTbl.addRow(addRow)

for(glfile of flist)
{
  log(glfile)
  let title = `Add to ${glfile}`
  addRow = new UITableRow()
  if(showContents)
  {
    title = `View ${glfile}`
  }
  
  addCell = addRow.addButton(title)
  addCell.dismissOnTap = true
  function ot(){
    targetFilePath = iCfiles.joinPath(glDirectory, glfile)
  }
  addCell.onTap = ot
  addCell.centerAligned()
  
  if(doAppend)
  {
    addCell= addRow.addButton("Add and view...")
    addCell.dismissOnTap = true
    addCell.centerAligned()
    addCell.onTap = () => {
      ot()
      showContents = true
    }
  }
  
  uiTbl.addRow(addRow)
}

await uiTbl.present(true)

while(toDos.length)
{
  await toDos.pop()()
}

sdOpts = {
  tables: true,
  tasklists: true,
  completeHTMLDocument: true
}


if(targetFilePath && doAppend)
{
  let addText = iCfiles.readString(targetFilePath)
  if(args.urls.length)
  {
    if(args.urls.length == args.plainTexts.length){
      for(const idx of Array(args.urls.length).keys())
      {
        addText += ` * [${args.plainTexts[idx]}](${args.urls[idx]})\n`
      }
    }
    else
    {
      for(url of args.urls)
      {
        addText += ` *  [${url}](${url})\n`
      }
      for(text of args.plainTexts)
      {
        addText += ` *  ${text}\n`
      }
    }
    for(img of args.images)
    {
      
    }
    
  }
  iCfiles.writeString(targetFilePath, addText)
}

if(showContents && targetFilePath)
{
  md = iCfiles.readString(targetFilePath)
  conv = new sd.Converter(sdOpts)
  
  html = conv.makeHtml(md)
  // Display the file.
  wv = new WebView()
  wv.shouldAllowRequest = ((request) => {
  //log(request)
  if(request["url"].startsWith("http"))
  {
    Safari.open("firefox-focus://open-url?url=" + encodeURIComponent(request["url"]), true)
    return false
  }
  return true
})
  await wv.loadHTML(html, iCfiles.getUTI(targetFilePath))
  await wv.present(true)
}

Script.setShortcutOutput(null)
Script.complete()

