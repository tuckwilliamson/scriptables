// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: pink; icon-glyph: magic;
// test pythonista callback handling. 

async function sleep(seconds) {
  if(seconds <= 0) {
    return
  }
  let t = new Timer()
  t.timeInterval = 1000*seconds
  t.repeats = false
  
  let p = new Promise((res,rej) => {t.schedule(function(){log("done");res("Done")})})
  return await p
}

let url = "pythonista3://Shrtcttr/helper.py"

cb = new CallbackURL(url)
cb.addParameter("action", "run")
let xcb = {"x-source":"Scriptable",
"ic-success" : "scriptable://x-callback-url/success",
"ic-error" : "scriptable://x-callback-url/error",
"ic-cancel" : "scriptable://x-callback-url/cancel"
}
cb.addParameter("argv", "xcbnext")
cb.addParameter("argv", JSON.stringify(xcb))

let base = cb.getURL()
log(base)

async function testCancel() {
  let cancelCb = new CallbackURL(base)
  cancelCb.addParameter("argv", "cancel")
  log(cancelCb.getURL())
  try {
    rtn = await cancelCb.open()
    logError("Cancel not handeled")
  }
  catch (err){
    log("Passed cancel")
    log(err)
  }
}

testCancel()

// log(cb.getURL())
cb.addParameter("argv", "error")
try {
  rtn = await cb.open()
  logError("Error not handeled")
}
catch(err){
  log("Passed error.")
  log(err)
}

// await sleep(5)

