// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: light-brown; icon-glyph: magic;
const apiTok = "b28e291fb7f1092b015264b68524159ad43fbb92"

const apiUrl = "https://api.todoist.com/rest/v1/"

let baseHead = {
  "Authorization":"Bearer " + apiTok,
  "Content-Type": "application/json",
}

const taskURI = apiUrl+"tasks"

async function getJSON(params, uri=taskURI) {
  let req = new Request(uri)
  baseHead["X-Request-Id"] = UUID.string()
  
  req.headers = baseHead
  req.method = "POST";
  
  req.body = JSON.stringify(params)
  
  return await req.loadJSON() 
}

let mainTitle = "Laundry every Saturday at 7am #Claire"
// if (args.shortcutParameter){
//   mainTitle = "Laundry #${args.shortcutParameter}
// }
const quickUri = "https://api.todoist.com/sync/v8/quick/add"

try {
  let mainTask = await getJSON({
   "text":mainTitle,
   "note":"Added by Scriptable"}, quickUri)
   
  log(mainTask)
  log(`Created main laundry task [${mainTask["id"]}]`)
  
  let wash = await getJSON({
   "content":"Wash", "parent":mainTask["id"]})
   
  let dry = await getJSON({
   "content":"Dry", "parent":mainTask["id"]})
   
  let fold = await getJSON({
   "content":"Fold", "parent":mainTask["id"]})
   
  let away = await getJSON({
   "content":"Put Away", "parent":mainTask["id"]})
   
   log(`Created sub tasks: 
   ${wash['id']}
   ${dry['id']}
   ${fold['id']}
   ${away['id']}`)
}
catch (err) {
  logError(err)
}
