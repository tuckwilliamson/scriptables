// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: red; icon-glyph: magic;
let js = `
module.exports = async ({ page }) => {
  
  await page.setRequestInterception(true);

  page.once("request", interceptedRequest => {
  
    page.setRequestInterception(false);
    
    interceptedRequest.continue({
      method: "POST",
      postData: "hdnCompanyId=1472&hdnCompanyIdentifier=sc911drivingschool&username=Wil9828&password=7243236",
      headers: {
        ...interceptedRequest.headers(),
        "Content-Type": "application/x-www-form-urlencoded"
      }
    });
  });

  const response = await page.goto("https://tds.ms/CentralizeSP/Student/Login/SC911drivingschool");
  
  const resp = await page.goto("https://tds.ms/CentralizeSP/BtwScheduling/Lessons?SchedulingTypeId=1");
  
  let rtn = await page.waitForFunction(
  'document.querySelector(".SelectAppointment").innerText');
  return {
    data: rtn,
    type: 'application/html',
  };
};
`
let rtn = js.replace(/\n/g, '');
log(rtn)

