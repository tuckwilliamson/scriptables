// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: cyan; icon-glyph: magic;
my = importModule('ScptTools')

log(my.settings)
log(my.settings._fName)

let defCal = await Calendar.defaultForReminders()

//let compList = await Reminder.completedToday([defCal])

let todoList = await Reminder.allIncomplete([defCal])
/*
log(todoList[0].creationDate)
log(toEpSec(todoList[0].creationDate))
*/
let lastSync = my.settings.get('lastSync',0)

let newTodos = todoList.filter(t=>t.creationDate.getTime()>lastSync)

if(newTodos.length < 1)
{
  Script.setShortcutOutput({success:true})
  Script.complete()
  return
}

let newLast = Math.max(...newTodos.map(t=>t.creationDate.getTime()))

log(`Last: ${lastSync} New: ${newLast} Cur new rem creation date: ${newTodos.map(t=>t.creationDate.getTime())}`)

tdList = JSON.parse(JSON.stringify(newTodos)).map(entry=>{
  entry.calendar = entry.calendar.title;
  return entry;})
log(tdList)

try
{
  req = new Request("https://hooks.zapier.com/hooks/catch/9624704/ojbp97k/")
  req.method = 'POST'
  req.body = JSON.stringify(tdList)
  rtn = await req.loadJSON()
  rtn.body = req.body
  rtn.resp = req.response
  
  my.settings.set('lastSync', newLast)
  log(`Synchronized ${tdList.length} reminders.`)
  rtn.success = true
  log(JSON.stringify(rtn, undefined, 2))  
  Script.setShortcutOutput(rtn)
}
catch(err)
{
  logError(err)
  Script.setShortcutOutput({success:false, error:err})
}

Script.complete()