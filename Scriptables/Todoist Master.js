// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: light-brown; icon-glyph: magic;
const apiTok = "b28e291fb7f1092b015264b68524159ad43fbb92"

const apiUrl = "https://api.todoist.com/rest/v1/"

let baseHead = {
  "Authorization":"Bearer " + apiTok,
  "Content-Type": "application/json",
}

const taskURI = apiUrl+"tasks"
const syncURI = "https://api.todoist.com/sync/v8/"
const quickUri = syncURI + "quick/add"

async function getJSON(params, uri=taskURI) {
  let req = new Request(uri)
  baseHead["X-Request-Id"] = UUID.string()
  
  req.headers = baseHead
  req.method = "POST";
  
  req.body = JSON.stringify(params)
  
  return await req.loadJSON() 
}

async function getLastAddedItem() {
  let uri = syncURI + "activity/get"
  
  let ret = await getJSON({ 
    "event_type": "items added",
    "limit" : 1}, uri)
  
}


var params = args.shortcutParameter

function exists(param) {
  return params !== null && 
    params[param] !== null && 
    params[param] !== undefined
}

function expect(param) {
  if (!exists(param)){
    throw(`Missing parameter "${param}".`)
  }
  return params[param]
}

const multipKey = "AllowMultiple"
const titleKey = "title"

if(config.runsInApp){
  //Testing
  params = {}
  params[titleKey] = "Test this out tomorrow at 9 & This too \n–Sub???"
  params[multipKey] = "&"
  
  log(params)
}

try {
  let title = expect(titleKey)
  let titles = [title]
  
  if (exists(multipKey) && 
    expect(multipKey)){
    
    let re = new RegExp("\\" + expect(multipKey) + "|\\n")
    if(re.test(title)){
      titles = title.split(re)
      log(titles)
    }
  }

  let lastTask = undefined
  let tasks = []
  for (title of titles) {
    log(`Adding :${title}`)
    
    lastTask = await getJSON({
   "text":title,
   "note":"Added by Scriptable"}, quickUri)
    tasks.push(lastTask)
    
    if(exists("loc")){
      //Todo
    }
  }
  
  rtn = JSON.stringify( {
  "success":`Created ${tasks.length} tasks!`,
  "tasks": tasks
  }, 2)
  log(rtn)
  return rtn
}
catch (err) {
  logError(err)
  if(config.runsInApp){
    let alt = new Alert()
    alt.title = err
    await alt.present()
  }
  else {
    return {"error":err}
  }
}
