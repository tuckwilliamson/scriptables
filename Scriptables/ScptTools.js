// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: gray; icon-glyph: magic;
/*
A module to natively store data for a Script.
 
*/
// 
// module.exports.settings = {}

let lcl = FileManager.local()
const curScriptSetFile = lcl.joinPath(lcl.documentsDirectory(),'.settings.' + Script.name())
module.exports.settingsFilePath = curScriptSetFile

class SettingsStore extends Map {
  
  constructor(fileName)
  {
    if(lcl.fileExists(fileName))
    {
      super(JSON.parse(lcl.readString(fileName)))
    }
    else
    {
      super()
    }
    this._fName = fileName
  }
  
  toJSON(key)
  {
    return [...this.entries()]
  }
  
  toString()
  {
    return `~Map(${this.toJSON().map(k=>`[${k}]`)})`
  }

  set(key, value)
  {
    super.set(key, value)
    
    // Save to disk now unless _fName
    // is undef - which happens in init
    if(this._fName)
      lcl.writeString(this._fName, 
      JSON.stringify(this, undefined, 2))
  }
  
  get(key, deflt=undefined)
  {
    if (this.has(key) )
    {
      return super.get(key)
    }
    else
    {
      return deflt
    }
  }
} 

function test()
{
  let tempTest = lcl.joinPath(
    lcl.temporaryDirectory(), UUID.string())

  t = new SettingsStore(tempTest)
  t.set('bob', 'tom')
  t.set(1, 'one')
  log(t.get('b','default'))
  t.forEach((v,k) => log(`${k}=${v}`))
  log(JSON.stringify(t))
  log(t)
  
  delete t
  log(lcl.readString(tempTest))
  t2 = new SettingsStore(tempTest)
  log(t2)
}

// test()
module.exports.settings = new SettingsStore(curScriptSetFile)