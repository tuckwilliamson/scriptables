// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: teal; icon-glyph: file-code;
function sleep(seconds, v) {
  if(seconds <= 0) {
    return
  }
  
  let scr = `
  console.log("Waiting ${seconds}");
  setTimeout(completion,${seconds*1000},${seconds});`
  log(scr)
  return v.evaluateJavaScript(scr, true)
}

let jhdDebugger = {
  style: `  <style type="text/css" media="screen">

  .jhdDebugger {
    width: 100%;
    background-color: lightgray;
    display: flex;
    flex-direction: column;
    align-items: stretch;
  }
  .ace_editor {
    /*position: relative !important;*/
    border: 1px solid lightgray;
    margin: auto;
    height: 300px;
    width: 80%;
    flex-grow: 5;
  }
  
  .buttonBar {
    flex-grow: 1;
    width 80%;
    display: flex;
    align-items: stretch;
  }
  
  .runbtn {
    /*position: relative !important;*/
    border: 2px solid green;
    width: 60%;
    height: 40px;
    background-color: lightgreen;
    flex-grow: 8;
    margin: 5px 0 0 10%;
  }
  
  .clearBtn {
/*    position: relative !important;*/
    border: 2px solid red;
    width: auto;
    height: 40px;
    background-color: orange;
    flex-grow: 2;
    margin: 5px 10% 0 10px;
  }
  
  .consoleErr {
    color: red;
    background-color: #ddd;
    text-decoration: underline;
  }
  
  .consoleScroll {
    flex-grow: 2;
    width: 80%;
    height: 150px;
    background: black;
    color: white;
    margin: 10px auto;
    padding: 2px;
    overflow: scroll;
    display: flex;
    flex-direction: column-reverse;
  }
  #console {
    width: 100%;
    background: #575757;
    color: white;
    padding: 1px;
  }
  </style>`,
  
  htm: `<div class="jhdDebugger">
<pre id="editor">
</pre>
<div class="buttonBar">
  <button class="runbtn" onclick="run()">Run</button>
  <button class="clearBtn" onclick="clearEditor()">Clear Script</button>
</div>
<div class="consoleScroll">
  <div id="console"></div>
  </div>
</div>

<script src="http://ajaxorg.github.io/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
<script>

var editor = ace.edit("editor");
editor.setTheme("http://ajaxorg.github.io/ace-builds/ace/theme/twilight");
editor.session.setMode("http://ajaxorg.github.io/ace-builds/ace/mode/javascript");
editor.commands.addCommand( {
  name: 'Run',
  bindKey: {win: 'Crrl-R', mac: 'Command-R'},
  exec: function runner(ed) {
    run();
  }})
  
function clearEditor() {
  editor.setValue('');
  editor.focus();
}
    
function _log(msg) {
  let d = new Date()
  var newline = document.createElement("div");
  newline.innerHTML = d.toLocaleTimeString() + " &gt; " + msg;
  document.querySelector("#console").appendChild(newline);
  return newline;
}

function log(msg) {
  console.log(msg);
  return _log(msg);
}

function error(msg) {
  console.log(msg)
  return _log("<span class='consoleErr'>" + msg + "</span>")
}

function jhdDisp(obj) {
  let ret = 'JHD: object <br />'
  for(var prop in obj) {
    ret += '  '+ prop +' (' + typeof(obj[prop]) + ') > ' + obj[prop] +'<br />';
  } 
  log(ret);
}

function run() {
  try {
    console.log(editor.getValue())
    eval( editor.getValue() )
  }
  catch(err) {
    console.log(err)
    error(err);
  }
}
</script>`,
  runJs: function(jsTxt, v) {
    return v.evaluateJavaScript(`try{
      ${jsTxt}
    }
    catch(err) {
      console.log(err);
    }
    finally {
      completion(true);
    }`, true);
  },
  
  inject: function(elementSelector, view) {
    let inject = `$('head').append(\`${this.style}\`);
      $('${elementSelector}').append(\`${this.htm}\`);`
    console.warn('Injecting jhdDebugger at ' + elementSelector + '.');
    return this.runJs(inject, view);
  },
  setScript: function(scr, view) {
    console.warn('Setting the script.');
    return view.afterLoad('editor.setValue(\`' + scr + '\`);');
  },
}

let baseUrl = "https://tds.ms/CentralizeSP/"

let r = new Request(baseUrl + "Student/Login/SC911drivingschool")

let form= {
  "hdnCompanyId": "1472",
  "hdnCompanyIdentifier" : "sc911drivingschool",
  "username" : "Wil9828",
  "password" : "7243236"
}

r.method = "POST"

for(param in form) {
  r.addParameterToMultipart(param, form[param])
}

//let data = await r.loadString()
v = new WebView()

v.afterLoad = function(scr) {
  let js = `try{ 
    document.addEventListener("DOMContentLoaded", function() { 
      try{
        ${scr}
      }
      finally {
        completion(true);
      } 
    });
  }
  catch(err) {
    console.log(err);
    completion(false);
  }`
  return v.evaluateJavaScript(js, true);
}
  
function allow(request) {
  console.log(`Requesting to load ${request.url}.`)
  return true;//request.url.startsWith('http')
}

v.shouldAllowRequest = allow

log("Logging in.")
await v.loadRequest(r)
log("Initial request loaded.")
log(r.body)
Pasteboard.copyString(r.body)
return
//await v.present(false)

//console.log(r.response)

//return

//sched = new Request(baseUrl + "BtwScheduling/Lessons?SchedulingTypeId=1")

v.shouldAllowRequest = allow
let loadP = v.loadURL(baseUrl + "BtwScheduling/Lessons?SchedulingTypeId=1")

v.shouldAllowRequest = allow
//let presentP = v.present()
log("Schedule loading....")

// let script = `
// console.log("Inside JS.");
// 
// let links = document.querySelectorAll('a');
// for (let link of links) { 
//   log(link);
// }
// `

  
let scr = `
console.log("Inside JS. Checking." + this.hasOwnProperty('ClearSearch').toString());

function gotData(data) {
  try {
    complete(JSON.stringify(data));
    console.log(data.toString());
  }
  catch(err) {
    console.log(err);
  }  
}

if (this.hasOwnProperty('ClearSearch') && this.hasOwnProperty('RefineSearch') ) {
    console.log("hijacking...");

    let cs = this.ClearSearch.toString().replace('RefineSearch(', 'return RefineSearch(');
    eval(cs);
    let rs = this.RefineSearch.toString().replace('return false', 'return request').replace('success: function (data) {', 'success: function (data) {gotData(data);');
    eval(rs.replace('async: true', 'async: false'));
    console.log(ClearSearch.toString());
    console.log(RefineSearch.toString());
    
    //let req = ClearSearch();
    ClearSearch();
    console.log("Waiting on ajax.");
    //await req;
    //console.log(req.responseText);
}
else {  
  let intCount=10
  
  let intID = setInterval( () => {
  try {
    console.log("Checking.");
    console.log(this.hasOwnProperty('ClearSearch'))
    eles = $('.SelectAppointment')
    if( intCount <= 0 || eles.length > 0)
    {
      console.log("Done Checking.");
      clearInterval(intID);
      
        eles.each( (ele) => {console.log(ele.innerText)})
        completion('Normal')
        return;
      
    }
    else {
      //let p = []
      //for (param in this.newrelic) {
        //p.push(param)
      //}
      //console.log(p)
      intCount = intCount - 1;
      //window.close()
    }
  }
  catch(err) {
    console.log(err)
  }
  finally {
    //completion('Final')
  }
    }, 500);
}
`
console.log('Trying to run JS.')
//await loadP;

//let jsP = v.evaluateJavaScript(scr, true)
let presP = v.present()
await v.waitForLoad().then( () => v.evaluateJavaScript('console.log("here")'))
let jsP = jhdDebugger.inject('body',v);


await Promise.race([jsP, presP]).then( (jsResp) => {
    console.log(jsResp)
  }).catch(function(err){
    console.warn("Error after load!");
    console.error(err)
  })
  
//let htm = await v.getHTML()

//rex = /data\-appointmentdatelongstring="(.*)"/g
//match = rex.exec(htm)
//log(match.length)
//v.present()
let htm = await v.getHTML()
Pasteboard.copy(htm)
