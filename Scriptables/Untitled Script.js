// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: gray; icon-glyph: magic;
let insrt = `
<pre id="editor">

</pre>

<script src="src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
<script>
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/twilight");
    editor.session.setMode("ace/mode/javascript");
</script>
`
let htm = `

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Editor</title>
  <style type="text/css" media="screen">

  .jhdDebugger {
    width: 100%;
    background-color: lightgray;
  }
  .ace_editor {
    position: relative !important;
    border: 1px solid lightgray;
    margin: auto;
    max-height: 70%;
    height: 300px;
    width: 80%;
  }
  
  .buttonBar {
    width 80%;
    display: flex;
    align-items: stretch;
  }
  
  .runbtn {
    border: 2px solid green;
    width: 60%;
    height: 40px;
    background-color: lightgreen;
    flex-grow: 8;
    margin: 5px 0 0 10%;
  }
  
  .clearBtn {
    border: 2px solid red;
    width: auto;
    height: 40px;
    background-color: orange;
    flex-grow: 2;
    margin: 5px 10% 0 10px;
  }
  
  .consoleErr {
    color: red;
    background-color: #ddd;
    text-decoration: underline;
  }
  
  .consoleScroll {
    width: 80%;
    height: 150px;
    background: black;
    color: white;
    margin: 10px auto;
    padding: 2px;
    overflow: scroll;
    display: flex;
    flex-direction: column-reverse;
  }
  #console {
    width: 100%;
    background: #575757;
    color: white;
    padding: 1px;
  }
  </style>
</head>
<body>

<div class="jhdDebugger">
<pre id="editor">
let p = []
for (var param in document){
    p.push(param)
}

let ls = JSON.stringify(p);
log(ls)
error(ls)
log(ls)
</pre>
<div class="buttonBar">
  <button class="runbtn" onclick="run()">Run</button>
  <button class="clearBtn" onclick="editor.setValue('')">Clear Script</button>
</div>
<div class="consoleScroll">
  <div id="console"></div>
  </div>
</div>

<script src="http://ajaxorg.github.io/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
<script>

var editor = ace.edit("editor");
editor.setTheme("http://ajaxorg.github.io/ace-builds/ace/theme/twilight");
editor.session.setMode("http://ajaxorg.github.io/ace-builds/ace/mode/javascript");
editor.commands.addCommand( {
  name: 'Run',
  bindKey: {win: 'Ctrl-R', mac: 'Command-R'},
  exec: function runner(ed) {
    run();
  }})
    
function _log(msg) {
  let d = new Date()
  var newline = document.createElement("div");
  newline.innerHTML = d.toLocaleTimeString() + " &gt; " + msg;
  document.querySelector("#console").appendChild(newline);
  return newline;
}

function log(msg) {
  console.log(msg);
  return _log(msg);
}

function error(msg) {
  console.error(msg)
  return _log("<span class='consoleErr'>" + msg + "</span>")
}


function run() {
  try {
    console.log(editor.getValue())
    eval( editor.getValue() )
  }
  catch(err) {
    console.error(err)
    error(err);
  }
}
</script>
</body>
</html>`

// Pasteboard.copyString(htm)

let v = new WebView()
v.loadHTML(htm, "http://ajaxorg.github.io/ace-builds/").
  then( ()=> v.present()).
  catch( (err) => {console.error(err)})

