// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: cyan; icon-glyph: magic;
let uri = "https://services2.arcgis.com/XZg2efAbaieYAXmu/arcgis/rest/services/COVID19_SharingView/FeatureServer/0/query?where=1%3D1&outFields=OBJECTID,NAME,Confirmed,Death,County_Rate,Probable_Positives,Probable_Deaths&returnGeometry=false&outSR=&f=json"

let weekAgo = new Date(Date.now() - 1000*60*60*24*7)


log(weekAgo)
// log(Date.now() - 1000*60*60*24*7)

function getDayInfo(daysAgo=0){
  
  let url = uri + "&time=" + String(Date.now() - 1000*60*60*24*daysAgo) //+ ",null"
  
  log(url)
  
  let req = new Request(url)
  
  return req.loadJSON();
  
//   log(data.features)
//   return data
 }

log((await getDayInfo(0)).features[0])
log((await getDayInfo(1)).features[0])
log((await getDayInfo(2)).features[0])