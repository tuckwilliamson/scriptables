// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: blue; icon-glyph: stethoscope;
let luxon = importModule("luxon-min")

const bkMrkName = "BPInfo"
let icfm = FileManager.iCloud()

if(!icfm.bookmarkExists(bkMrkName))
  throw "BP file bookmark doesn't exist???"

// log(icfm.bookmarkedPath(bkMrkName))

function avg(data){
  return Math.round( data.reduce( (a,b) => {return (a+b)}, 0)/data.length)
}
function shortcutDuration(durStr) {
  let elets = durStr.split(':').reverse()
  // console.log(elets)
  const keys = ['seconds', 'minutes', 'hours', 'days']
  let durObj = keys.reduce( (obj, cur, idx, origArr) => {
    if(idx<elets.length){
      obj[cur] = elets[idx]
    }
    return obj
  }, {})
  // console.log(durObj)
  return luxon.Duration.fromObject(durObj)
}
// log(shortcutDuration('1:20'))
const today = new luxon.DateTime.local()

class Reading{
  constructor(data)
  {
    this.data = data;
    this.ae = undefined;
  }
  get raw(){
    return this.data
  }
  get date(){
    return this.data.Date;
  }
  set ActiveEnergy(value){
    this.ae = value
  }
  get ActiveEnergy(){
    return this.ae
  }
  get ms(){
    return this.date.toMillis()
  }
  get ago(){
    return luxon.Interval.fromDateTimes(this.date, today)
  }
  get daysAgo(){
    return Math.floor(this.ago.length("days"))
  }
}
class BPReading extends Reading{
  // constructor(data)
  // {
  //   this.data = data;
  // }
  static _rate(value, bounds){
    // Returns an index of the bounds array that the value is less than.
    let last = 0;
    for (var bound = 0; bound < bounds.length; bound++) {
      if(last <= value && value < bounds[bound])
        return bound;
    }
    return null;
  }
  get raw(){
    return {systolic: this.systolic, diastolic: this.diastolic}
  }
  get systolicRating(){
    //Returns a number giving the rating of the systolic reading.
    // 0: Normal range, 1: Stage 1, 2: Stage 2, 3: crisis
    // good: <120, elevated: <130, stage 1: < 140, stage 2: < 180, crisis: >= 180
    const bounds = [120, 130, 140, 180,111111]
    return BPReading._rate(this.systolic, bounds)
  }
  get systolic(){
    return this.data.Sys;
  }
  get diastolicRating(){
    // good: <80, elevated: <80, stage 1: < 90, stage 2: < 120, crisis: >= 120
    const bounds = [80, 90, 120, 111111]
    return BPReading._rate(this.diastolic, bounds)
  }
  get diastolic(){
    return this.data.Dia;
  }
}
class BPDataManager {
  constructor(){
    this.path = icfm.bookmarkedPath(bkMrkName)
    if(true)
    {
      this.raw = icfm.readString(this.path)
      this.rawJson = JSON.parse(this.raw)
    }
    else
    {
      let hrURL = new CallbackURL("shortcuts://run-shortcut")
      hrURL.addParameter("name", "BPInfo")
      hrURL.addParameter("input", "")
      this.rawJson = hrURL.open()
      log(this.rawJson)
    }
    //this.raw = this.raw.replaceAll("”",'"')


    // Because of Shortcut limits there are
    // multiple arrays for each data point
    // here we stitch them together
    let dataArrays = []
    for (var prop in this.rawJson) {
      let pVal = this.rawJson[prop]
      if (Array.isArray(pVal)) {
        dataArrays.push(prop)
      }
    }
    const keyProp = "ReadingDateTime"
    let data = []
    for (var i = 0; i < this.rawJson[keyProp].length; i++) {
      let curMap = {}
      for(var key of dataArrays){
        curMap[key] = this.rawJson[key][i]
      }
      let dte = this.rawJson[keyProp][i]
      dte = luxon.DateTime.fromISO(dte)
      curMap.Date = dte
      data.push([dte.toMillis(), new BPReading(curMap)])
    }

    this.data = new Map(data)
    this.byNewest = [...this.data.keys()].sort().reverse()

    log(this.rawJson.Other)
    this.rawJson.Other = JSON.parse(this.rawJson.Other.replaceAll('\n', '","').replaceAll("”",'"'))
    //Process workouts.
    this._processWorkouts()
    //log(JSON.stringify( [...this.workoutMap.entries()], undefined, 2))

    //Process meditation.
    this._initMeditation()
    // log(this.meditationData)
    // log(this.meditationData.Date)
  }

  /** This method initializes all workout data. Some of which is pulled from
   * multiple workout sources (Peloton, Cycling += ActiveEnergy Workout).
   * this.workoutMap is the final combination of all workouts.
   */
  _processWorkouts()
  {
    this._initPeloton()
    this._initCycling()
    let [data, map] = this._initWorkout(this.rawJson.Other.Workouts)
    //Value is actually active energy for this.
    this.activeEnergyMap = map

    this._combineWorkouts()
  }
  _combineWorkouts()
  {
    this.workoutMap = new Map(this.cyclingMap.entries())
    let latest = this.cyclingMap.get("latest").ms
    for( let cur of this.pelotonMap.entries())
    {
      let [key, val] = cur
      if(key === 'latest')
        continue

      if(key > latest)
        latest = key

      this.workoutMap.set(key, val)
    }
    //Combine current entries with their active energy if they are already there.
    for (let cur of this.activeEnergyMap.entries())
    {
      let [key, val] = cur
      val.ActiveEnergy = val.raw.Value
      if(key === 'latest')
        continue;
      if(key > latest)
        latest = key

      if(this.workoutMap.has(key))
      {
        let updated = this.workoutMap.get(key)
        updated.ActiveEnergy = val.ActiveEnergy
        this.workoutMap.set(key, updated)
        log(this.workoutMap.get(key).ActiveEnergy)
      }
      else
      {
        this.workoutMap.set(key, val)
      }
    }
    this.workoutMap.set("latest", this.workoutMap.get(latest))
  }
  _initWorkout(workoutData, url = "https://members.onepeloton.com/classes")
  {
    //tempD = tempD.replaceAll('\n', '","').replaceAll("”",'"')
    //let workoutData = JSON.parse(tempD)

    let latest = 0
    //Create a map from the data.
    let workoutMap = workoutData.Date.reduce( (mp, cur, idx, orgArr) => {
      let dte = luxon.DateTime.fromRFC2822(cur)
      let key = dte.toMillis()
      if(key > latest) latest = key

      mp.set(key , new Reading({"Date":dte,
        "Duration": workoutData.Duration[idx],
        "Value": workoutData.Source[idx],
        "RawValue": new Number( workoutData.Distance[idx]).toFixed(2),
        "Url":url,
        "Icon":workoutData.Icon}))
      return mp
    }, new Map())
    //Set latest
    workoutMap.set("latest", workoutMap.get(latest))
    //log([...workoutMap.entries()])

    return [workoutData, workoutMap]
  }
  _initPeloton()
  {

    let [data, map] = this._initWorkout(this.rawJson.Other.Peloton)
    this.pelotonData = data
    this.pelotonMap = map
  }
  _initCycling()
  {
    let [data, map] = this._initWorkout(this.rawJson.Other.Cycling)
    this.cyclingData = data
    this.cyclingMap = map
  }
  _initMeditation()
  {
    this.meditationData = this.rawJson.Other.Meditation
    let latest = 0
    //Create a map from the data. Roll together all durations.
    this.meditationMap = this.meditationData.Date.reduce( (mp, cur, idx, orgArr) => {
      let dte = luxon.DateTime.fromRFC2822(cur)
      let key = dte.startOf('day').toMillis()
      if(key > latest) latest = key

      let dur = shortcutDuration(this.meditationData.Duration[idx])
      if(mp.has(key))
      {
        let rec = mp.get(key)
        rec.raw.Duration = rec.raw.Duration.plus(dur).normalize()

      } else {
        mp.set(key, new Reading({"Date":dte, "Duration":dur}))
      }
      return mp
    }, new Map())
    //Set latest
    this.meditationMap.set("latest", this.meditationMap.get(latest))
    //log([...this.meditationMap.entries()])
  }
  get latestMeditation()
  {
    return this.meditationMap.get('latest')
  }
  get latestWorkout()
  {
    return this.workoutMap.get('latest')
  }
  get latest(){
    return this.data.get(this.byNewest[0])
  }
  get average()
  {
    return new BPReading({Sys: avg(this.rawJson.Sys), Dia: avg(this.rawJson.Dia), Date: today})
  }
  get weekAverage()
  {
    return this.averageForDays(7)
  }
  get twoWeekAverage()
  {
    return this.averageForDays(14)
  }
  averageForDays(daysAgo)
  {
    let cutoff = today.startOf('day').minus({days:daysAgo}).toMillis()
    let sys = []
    let dia = []
    for (var readDate of this.byNewest) {
      if (readDate < cutoff)
      {
        //It's by date reverse so once we reach older dates just stop iterating.
        break;
      }
      if (this.data.has(readDate)) {
        let bpRead = this.data.get(readDate)
        sys.push(bpRead.systolic)
        dia.push(bpRead.diastolic)
      } else {
        console.warn(`Missing BP reading for key: ${readDate} while averaging.`)
      }
    }
    if(sys.length && dia.length)
    {
      return new BPReading({Sys:avg(sys), Dia:avg(dia), Date: today})
    }
    else
    {
      return undefined
    }
  }
  get max(){
    return new BPReading({Sys: Math.max(...this.rawJson.Sys), Dia: Math.max(...this.rawJson.Dia), Date: today})
  }
  get min(){
    return new BPReading({Sys: Math.min(...this.rawJson.Sys), Dia: Math.min(...this.rawJson.Dia), Date: today})
  }
}

let bpdm = new BPDataManager()

let hrURL = "shortcuts://run-shortcut?name=BPInfo&input=BP"

log(hrURL)

let defFont = Font.lightMonospacedSystemFont(13)
let minScFac = 0.2

switch(config.widgetFamily){
  case "small":
    break
  case "medium":
    minScFac = 0.1
    defFont = Font.lightMonospacedSystemFont(15)
    break
  default:
    defFont = Font.lightMonospacedSystemFont(18)
    minScFac = 0.5
}
//  log([...Object.entries(defFont)])
const defCnrRad = 5
let view = new ListWidget()
let hrow = view.addStack()
view.addSpacer()

function getColor(rating)
{
  switch(rating){
    case 0:
      return Color.green()
    case 1:
      return Color.yellow()
    case 2:
      return Color.orange()
    case 3:
      return Color.red()
    default:
      return Color.magenta()
  }
}
function addPoint(row, point, rating, spacer=true){
  if(spacer) row.addSpacer()
  let s = row.addText(String(point))
  s.minimumScaleFactor = minScFac
  s.textColor = getColor(rating)
  s.cornerRadius = defCnrRad
  s.centerAlignText()
  s.font = defFont
}

function addDataV(title, data){
  trow.addSpacer()
  let t = trow.addText(title)
  t.minimumScaleFactor = minScFac/2
  t.centerAlignText()
  let rating = Math.max(data.systolicRating, data.diastolicRating)
  t.textColor = getColor(rating)
  t.shadowColor = Color.black()
  t.shadowRadius = 2
  t.shadowOffset = new Point(0.5, 0.5)
  t.font = defFont

  addPoint(sysRow, data.systolic, data.systolicRating)
  addPoint(diaRow, data.diastolic, data.diastolicRating)
  return rating
}
function addDataH(title, data){
  let dataRow = view.addStack()
  let t = dataRow.addText(title)
  t.minimumScaleFactor = minScFac/2
  t.rightAlignText()
  let rating = Math.max(data.systolicRating, data.diastolicRating)
  t.textColor = getColor(rating)
  // t.shadowColor = Color.black()
  // t.shadowRadius = 2
  // t.shadowOffset = new Point(0.5, 0.5)
  t.font = defFont

  addPoint(dataRow, data.systolic, data.systolicRating)
  addPoint(dataRow, data.diastolic, data.diastolicRating)
  return rating
}
let addData = addDataV

function setupH() {
  var thdrRow = view.addStack()
  thdrRow.backgroundColor = Color.white()
  thdrRow.borderColor = Color.white()
  thdrRow.borderWidth = 1
  let t = thdrRow.addText('     ')
  t.minimumScaleFactor = minScFac/2
  t.font = defFont

  thdrRow.addSpacer()
  t = thdrRow.addText('Sys')
  t.minimumScaleFactor = minScFac/2
  t.textColor = Color.black()
  t.font = defFont

  thdrRow.addSpacer()
  t = thdrRow.addText('Dia')
  t.minimumScaleFactor = minScFac/2
  t.textColor = Color.black()
  t.font = defFont

  addData = addDataH
}
function setupV() {
  var trow = view.addStack()
  trow.backgroundColor = Color.white()
  trow.borderWidth = 1
  trow.borderColor = Color.white()
  var sysRow = view.addStack()
  var diaRow = view.addStack()
  diaRow.backgroundColor = Color.gray()

  let t = trow.addText('Value')
  t.minimumScaleFactor = minScFac/2
  t.textColor = Color.black()
  t.font = defFont

  t = sysRow.addText("Sys")
  t.rightAlignText()
  t.font = defFont

  t = diaRow.addText("Dia")
  t.rightAlignText()
  t.font = defFont
}
switch(config.widgetFamily){
  case "small":
  case null:
    setupH()
    break
  default:
    setupH()
}

let hrating = addData('Last', bpdm.latest)
addData('Avg ', bpdm.average)
addData('Max ', bpdm.max)
addData('Min ', bpdm.min)
if(config.widgetFamily != 'small')
{
  addData('7d Avg', bpdm.weekAverage)
  addData('14d Avg', bpdm.twoWeekAverage)
}
function addWorkout() {
  // Peloton
  let pRow = view.addStack()
  let pData = bpdm.latestWorkout

  if(!pData) return 99

  if(pData.raw.Url)
    pRow.url = pData.raw.Url

  addPoint(pRow, pData.data.Icon, pData.daysAgo, false)
  let pDte = pRow.addDate(pData.date.toJSDate())
  if(config.widgetFamily == "small") pDte.applyOffsetStyle()
  else pDte.applyRelativeStyle()
  pDte.font = defFont
  pDte.minimumScaleFactor = minScFac/2
  pDte.textColor = getColor(pData.daysAgo)
  if(config.widgetFamily != "small"){
    addPoint(pRow, pData.raw.Value, pData.daysAgo)
  } else {
    addPoint(pRow, '', 0)
  }
  addPoint(pRow, pData.raw.Duration, pData.daysAgo)
  return pData.daysAgo
}
function addMeditation() {
  // Meditation
  let mRow = view.addStack()
  let mData = bpdm.latestMeditation
  addPoint(mRow, "🏵", mData.daysAgo, false)
  let mDte = mRow.addDate(mData.date.toJSDate())
  if(config.widgetFamily == "small") mDte.applyOffsetStyle()
  else mDte.applyRelativeStyle()
  mDte.font = defFont
  mDte.minimumScaleFactor = minScFac/2
  mDte.textColor = getColor(mData.daysAgo)
  addPoint(mRow, "", mData.daysAgo)
  addPoint(mRow, mData.raw.Duration.toFormat('h:mm:ss'), mData.daysAgo)
  return mData.daysAgo
}
addWorkout()
addMeditation()

let colors = []
hrow.backgroundGradient = new LinearGradient()
hrow.backgroundGradient.colors.push(Color.black())
function addHeader(rec, pre, spacer = true) {
  hrow.backgroundGradient.colors.push(getColor(rec.daysAgo))


  let t = hrow.addText(pre)
  t.textColor = getColor(rec.daysAgo)
  t.minimumScaleFactor = minScFac
  t.font = defFont

  t = hrow.addDate(rec.date.toJSDate())
  t.applyOffsetStyle()
  t.textColor = getColor(rec.daysAgo)
  t.minimumScaleFactor = minScFac
  t.font = defFont
  if(spacer) hrow.addSpacer()

  return t
}
addHeader(bpdm.latest,'🫀').url = hrURL

if(config.widgetFamily != "small")
{
  let curWkout = bpdm.latestWorkout
  addHeader(curWkout, curWkout.data.Icon).url = curWkout.raw.Url
  addHeader(bpdm.latestMeditation,'🏵')
}

// view.url = url
Script.setWidget(view)
if(!config.runsInWidget)
{
  view.presentLarge()
}
else
{
  view.url = URLScheme.forRunningScript()
  view.refreshAfterDate = today.plus({minutes:10}).toJSDate()
  switch(config.widgetFamily)
  {
    case "large":
      view.presentLarge()
      break
    case "medium":
      view.presentMedium()
      break
    default:
      view.presentSmall()
  }
}
