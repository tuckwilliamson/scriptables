// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: deep-green; icon-glyph: chart-area;
var luxon = importModule("luxon-min")
let htm = null
let url = "https://scdhec.gov/covid19"
let debug = false
icfm = FileManager.iCloud()
fpath = icfm.documentsDirectory() + "/cvd.html"

function hours(hours){
  return Date.now() + (hours*60*60*1000)
}

class PyMap extends Map
{
  get_def(key, defaultVal){
    if(!this.has(key)){
      this.set(key, defaultVal)
    }
    return this.get(key);
  }
  toString(){
    let retVal = "Map(["
    for(let [key,val] of this.entries())
    {
      retVal+=`["${key}",${val.toString()}],`
    }
    return retVal+"])"
  }
}
class CountyDataLoader
{
  constructor()
  {
    let cutoff = luxon.DateTime.local().minus({days:14}).toMillis()
    let whereComp = `County is not null`
    this.url = "https://services2.arcgis.com/XZg2efAbaieYAXmu/arcgis/rest/services/COVID_19_Time_Cases_Eastern_View/FeatureServer/0/query?outFields=County,Deaths,Cases,Positive_Cases,Date,FID&returnGeometry=false&outSR=4326&f=json&resultRecordCount=1000&orderByFields=Date+DESC&where=" + encodeURIComponent(whereComp)
    //log(this.url)
    this.data = null
    this.reqPromise = null
    this.ordered = null
  }
  load(){
    this.reqPromise = new Request(this.url).loadJSON()
    return this.reqPromise.then( (jsonData) => {
      this.data = jsonData;
      return this.data;
    }).catch( (err) => {
      this.data = {
        features: { attributes: {
            Error: err,
            Name: "⚠️Error!",
            Date: Date.now(),
            Deaths: null,
            Cases: JSON.stringify(err),
          }
        }
      }
      logError(err);
    })
  }
  get jdata()
  {
    if (!this.data) {
      throw "Data not loaded. You should call load() and either await the returned promise, or use .then on the returned promise."
    }
    return this.data
  }
  get byDate()
  {
    if(this.ordered)
    {
      return this.ordered;
    }
    let data = new PyMap();
    //log(this.jdata)
    for (var cntyRaw of this.jdata.features) {
      cntyRaw = cntyRaw.attributes;
      let dte = luxon.DateTime.fromMillis(cntyRaw.Date).startOf('day');  
      cntyRaw.Date = dte
      dte = dte.toMillis()
      if(!data.has(dte))
      {
        data.set(dte, new PyMap([[cntyRaw.County, cntyRaw]]))
      } else {
        data.get(dte).set(cntyRaw.County, cntyRaw);
      }
    }
    this.ordered = data;
    this.dates = [...data.keys()].sort( (a,b) => {
      if(b<a){
        return -1;
      }
      return 1
    })
    let cutoff = luxon.DateTime.local().minus({days:14}).startOf("day").toMillis()
    for(let dte of this.dates)
    {
      if(dte < cutoff) break
      let data = this.ordered.get(dte)
//       log([...data.values()][0])
      log(`Date:${data.get("Charleston").Date}-(${data.size})-${[...this.ordered.get(dte).values()].reduce( (acc,cur) => acc+cur.Cases,0)}`)
    }
    return this.ordered;
  }
  forDate(dte){
    dte = dte.startOf("day").toMillis()
    return this.byDate.get(dte)
  }
  get today()
  {
    return this.forDate(luxon.DateTime.local())
  }
  get yesterday()
  {
    return this.forDate(luxon.DateTime.local().minus({days:1}))
  }
  get latest()
  {
    return this.byDate.get(this.dates[0])
  }
  toString(){
    let retVal = "{["
    for(var dte of this.dates){
      retVal += `[${luxon.DateTime.fromMillis(dte).toLocaleString()},${[...this.ordered.get(dte)]}],`
    }
    retVal += "]}"
    return retVal
  }
}
let ctyDataLdr = new CountyDataLoader()
let jdata = await ctyDataLdr.load()
// log([...ctyDataLdr.ordered.entries()])
// ctyDataLdr.byDate
// log(luxon.DateTime.fromMillis(ctyDataLdr.dates[0]))
// log([...ctyDataLdr.ordered.get(ctyDataLdr.dates[0])])
// log(ctyDataLdr.latest)
// log(ctyDataLdr)
// throw "barf"

let countyData = ctyDataLdr.latest

class HistoricalData
{
  constructor(dataName)
  {
    const dataDir = icfm.documentsDirectory()+"/HistData"
    this.path = HistoricalData.dataDir+`/${dataName}.json`
    this.data = {}
    if(!icfm.isDirectory(HistoricalData.dataDir))
    {
      icfm.createDirectory(HistoricalData.dataDir, true)
    }
    if(icfm.fileExists(this.path))
    {
      this.data = JSON.parse(icfm.readString(this.path))
    }
  }
}

if(icfm.fileExists(fpath) && debug)
{
  htm = icfm.readString(fpath)
}
else
{
  htm = await new Request(url).loadString()
  //let lcl = FileManager.local()
  icfm.writeString(icfm.documentsDirectory()+"/Sccvd.html", htm)
}


dateMatch = htm.matchAll(/Reported \w*, (\w*) (\d*), (\d*) (\d*):(\d*).*(p|a)\.m/g).next()


let [ctx, mnth, day, year, hour, min, ampm] = dateMatch.value
hour=new Number(hour)
if("p" == ampm && hour < 12)
{
  hour+=12
}
let dteStr = `${mnth} ${day} ${year} ${hour}:${min}`
log(dteStr)

let statDate = new Date(dteStr)
let refDate = new Date(statDate.valueOf() + (23*60*60*1000))
if(refDate < Date.now()){
  //Start checking every 15 min.
  refDate = new Date(Date.now() + (15*60*1000))
//   log(`start checking more frequently: next check ${refDate.toTimeString()}`)
}
//throw "up"

m = htm.matchAll(/([\s\w\&]*)(?:\<\/a\>)?\<\/p\>\n.*\n\<span class="stat"\>(.*)\<\/span\>\<\/div\>/g)

let stats = {} //{"date":statDate}
let numStats = 0
for(match of m)
{
  let [na, stat, data] = match
  stats[stat] = data
  numStats++
}
log(JSON.stringify(stats, null, 2))

let view = new ListWidget()
let head = view.addStack()
head.addDate(statDate).minimumScaleFactor=0.5
head.addSpacer(20)
let timer = head.addDate(refDate)
timer.minimumScaleFactor = 0.2
timer.applyTimerStyle()
timer.rightAlignText()
timer.cornerRadius = 5
timer.backgroundColor = Color.white()

size = config.widgetFamily

//log([...countyData.entries()])
function addCounty()
{
  if(!countyData)
    return
    
  let minScFac = 0.05
  let row = view.addStack()
  let bgGrad = new LinearGradient()
  bgGrad.startPoint=new Point(0, 0)
  bgGrad.endPoint=new Point(1,0)
  bgGrad.colors = [Color.darkGray(), Color.orange()]
  bgGrad.locations = [0.3,0.7]
  row.backgroundGradient = bgGrad
  row.cornerRadius=5
  totalCases=0
  firstCty = true
  for(key of countyData.keys()){
    switch (key) {
      case "Dorchester":
      case "Berkeley":
      case "Charleston": 
        break
      default:
        continue;
    }
    let data = countyData.get(key)
    if(firstCty)
    {
      firstCty = false
      row.addSpacer(2)
      let dte = row.addDate(data.Date.toJSDate())
      dte.applyOffsetStyle()
      dte.minimumScaleFactor = minScFac
    }
    
    totalCases += data.Cases
    var addTxt = function(dispTxt)
    {
      row.addSpacer(10)
      let txt = row.addText(dispTxt)
      txt.minimumScaleFactor = minScFac
      //row.addSpacer(10)
      //txt = row.addText()
      txt.backgroundColor= Color.lightGray()
      txt.centerAlignText()
      txt.cornerRadius = 5
      txt.borderWidth=1
      txt.borderColor= Color.blue()
    }
    addTxt(`${key.slice(0,3)} :: ${data.Cases}`)
  }
  if (totalCases)
  {
    row.addSpacer()
    addTxt(`Tot::${totalCases}`)
  }
  row.addSpacer(3)
}
if(size != "small"){
  addCounty()
}

for(prop in stats)
{
  let lowprop = prop.toLowerCase()
  txt = prop
  switch(size)
  {
    case "small":
      if(lowprop.includes("probable"))
        continue
      txt = txt.replace(/new /ig, "")
    case "medium":
      if(lowprop.startsWith("total"))
        continue
  }

  let row =view.addStack()
  t1= row.addText(txt)
  t1.leftAlignText()
  t1.minimumScaleFactor = 0.3
  if(size == 1)
  {
    row = view.addStack()
    row.backgroundColor = Color.darkGray()
    row.cornerRadius = 5
    row.addSpacer(null)
  }
  else
  {
    row.addSpacer(null)
  }
  t2 = row.addText(stats[prop])
  t2.rightAlignText()
  t2.minimumScaleFactor = 0.3
}

/*let ttUrl = new CallbackURL("textastic://x-callback-url/replace")
ttUrl.addParameter("location", "local")
ttUrl.addParameter("name", "SCCVD.html")
ttUrl.addParameter("text", htm)
view.url = ttUrl.getURL()
log(view.url)*/
view.url = url
Script.setWidget(view)
if(!config.runsInWidget)
{
  //view.presentMedium().then( () => {return view.presentSmall()})
  
  view.presentMedium()
}
else
{
  view.refreshAfterDate = refDate
  switch(size)
  {
    case "large":
      view.presentLarge()
      break
    case "medium":
      view.presentMedium()
      break
    default:
      view.presentSmall()
  }
}
