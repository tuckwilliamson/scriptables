// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: green; icon-glyph: code; share-sheet-inputs: url, plain-text, file-url;
log(args.plainTexts)
var url = args.urls && args.urls.length ? args.urls[0] : "https://google.com"

var req = new Request(url)

let src = await req.loadString()

// log(src)
urlParts = url.split("/")

alt = new Alert()
alt.addAction("Open in Textastic...")
alt.addAction("Save to Scriptable iCloud...")
alt.addTextField("Enter file name if saving.", urlParts[urlParts.length-1])
alt.title = "Open in or Save..."
usrChoice = await alt.presentAlert()

if(usrChoice)
{
  icMan = FileManager.iCloud()
  fileName = icMan.joinPath(icMan.documentsDirectory(), alt.textFieldValue(0))
  if(icMan.fileExists(fileName))
  {
    alt = new Alert()
    alt.addAction("okay")
    alt.title = "File exists"
    alt.message = "Aborting because the file exists."
    await alt.presentAlert()
    Script.complete()
  }
  else
  {
    icMan.writeString(fileName, src)
  }
}
else
{
  xcb = new CallbackURL("textastic://x-callback-url/new?")
  xcb.addParameter("name", "Source.html")
  xcb.addParameter("text", src)
  
  let rtn = await xcb.open()
  log(rtn)
}

Script.setShortcutOutput(null)
Script.complete()