// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// always-run-in-app: true; icon-color: yellow;
// icon-glyph: magic; share-sheet-inputs: url, plain-text;

let rcals = await Calendar.forReminders()
let asl = []
for(let cal of rcals){
  log(cal.title)
  if(cal.title.startsWith("Alexa"))
    asl.push(cal)
}
let comp = await Reminder.allCompleted(rcals)
let aslR = await Reminder.all(asl)
log(aslR.length)
for(r of aslR)
{
  r.remove()
}
throw 'barf'
var sargs = {}
sargs.shortcutParameter = "Clipboard"

var rtn = {}
rtn.args = sargs
rtn.errors = []

function FinishScript(error = null)
{
  if(error)
  {
    rtn.errors.push("User cancelled key add.")
  }
  Script.setShortcutOutput(rtn)
  Script.complete()
}

if(!Keychain.contains("gapiKey"))
{
  let alt = new Alert()
  alt.title = "Please enter your Google API Key"
  alt.addSecureTextField("key")
  alt.addAction("OK")
  alt.addCancelAction("Cancel")
  let res = await alt.presentAlert()
  if(res)
  {
    FinishScript("User cancelled key add.")
    return
  }
  let key = alt.textFieldValue(0).trim()
  if(key !== encodeURIComponent(key)|| key.length < 39)
  {
    FinishScript("Key entered is invalid."+key.length)
    return
  }
  else
  {
    Keychain.set("gapiKey", key)
  }
}

htm = await new Request(sargs.shortcutParameter).loadString()
let mtch = htm.matchAll(/\<title\>(.*)\&.*?;(.*)\&.*\<\/title\>/gi).next()
rtn.title = mtch.value[1].trim()
rtn.author = mtch.value[2].trim()

function getDuration(matchArray)
{
  //This takes a match return
  // [all, h, m, s] | [all, null, m, s] | [all, null, null, s] and
  // returns the total seconds.
  const multipliers = [60*60, 60, 1, 60, 1, 1]
  matchArray = matchArray.filter( (cur, idx)=> idx > 0).map(cur => {return cur ? Number(cur.replace(':','')) : 0})
  console.log(matchArray)
  return matchArray.reduce( (acc, cur, idx) => {return acc + (multipliers[idx]*cur)}, 0)
}

rtn.mtch = sargs.shortcutParameter.match(/(\d+):(\d+):(\d+)|(\d+):(\d+)|(\d+)$/)
if(rtn.mtch)
{
  rtn.podTime = getDuration(rtn.mtch)
  rtn.timeCode = rtn.podTime + 's'
}
else
{
  rtn.podTime = 10
  rtn.timeCode = "10s"
}

rtn.usrWHSearch = `https://www.youtube.com/feed/history?query=${encodeURIComponent(rtn.title)}`

// Look up data in YouTube to find if this has a video.
function gAPIRequest(verb, params)
{
  params.key = Keychain.get("gapiKey")
  //Get the params as an array of ["a=b", "c=d", ...]
  let paramArr = Object.entries(params).reduce( (acc, cur) => {
    acc.push(cur[0] + "=" + encodeURIComponent(cur[1]))
    return acc}, [])
  return new Request(`https://www.googleapis.com/youtube/v3/${verb}?${paramArr.join('&')}`)
}
//let jsonDataReq = new Request(`https://www.googleapis.com/youtube/v3/search?q=${encodeURIComponent(rtn.author +" "+rtn.title)}&key=${Keychain.get("gapiKey")}&type=video&maxResults=1&part=snippet,id`)
let jsonDataReq = gAPIRequest('search', {q:rtn.author +" "+ rtn.title,
  type:'video', maxResults:1})
rtn.ytSearch = jsonDataReq.url

rtn.ytJson = await jsonDataReq.loadJSON()
if(rtn.ytJson && rtn.ytJson.items && rtn.ytJson.items.length && rtn.ytJson.items[0].id && rtn.ytJson.items[0].id.videoId)
{
  rtn.vidId = rtn.ytJson.items[0].id.videoId
  rtn.youtubeUrl = `https://www.youtube.com/watch?v=${rtn.vidId}&t=${rtn.timeCode}`

  //Get details of the video.
  jsonDataReq = gAPIRequest('videos', {id:rtn.vidId, part: 'contentDetails,player'})
  rtn.ytDetails = await jsonDataReq.loadJSON()
  if(rtn.ytDetails && rtn.ytDetails.items && rtn.ytDetails.items.length &&
      rtn.ytDetails.items[0].contentDetails &&
      rtn.ytDetails.items[0].contentDetails &&
      rtn.ytDetails.items[0].contentDetails.duration)
  {
    rtn.duration = rtn.ytDetails.items[0].contentDetails.duration.match(/PT(?:(\d+)H)?(?:(\d+)M)?(?:(\d+)S)?/)
    rtn.durationSeconds = getDuration(rtn.duration)
  }
}

let wv = new WebView()
let _ = await wv.loadURL(rtn.usrWHSearch)
rtn.html = await wv.getHTML()
//_ = await wv.present(true)

rtn.history = []
for(let src of rtn.html.matchAll(/href="(\/watch\?v=.*?)"/g))
{
  //log((src[1]))
  rtn.history.push(src[1])
}
if(rtn.vidId)
{
  rtn.curPos = rtn.html.match(`href="\\/watch\\?v=${rtn.vidId}(.*?)?"`)
  if(rtn.curPos)
  {
    rtn.curPos = rtn.curPos.filter(a=>a)
    if(rtn.curPos.length > 1)
    {
      rtn.ytTime = rtn.curPos[1].match(/(\d+)s/)
      if(rtn.ytTime)
        rtn.ytTime = Number(rtn.ytTime[1])
    }
    else
    {
      //Finished in yt, make ytTime Infinity
      rtn.ytTime = rtn.durationSeconds ? rtn.durationSeconds : Infinity
    }
  }
}
else {
  rtn.ytTime = 0
}
rtn.html = undefined

if(rtn.podTime && rtn.ytTime)
{
  rtn.ytTime += 90
  rtn.curTime = Math.max(rtn.podTime,rtn.ytTime)
}
else
{
  rtn.curTime = rtn.podTime ? rtn.podTime : rtn.ytTime
}

if(rtn.curTime === rtn.podTime)
{
  rtn.defaultUrl = rtn.youtubeUrl
  rtn.podUrl = sargs.shortcutParameter
}
else
{
  const hours = 60*60
  rtn.podTimeStr = Math.floor(rtn.ytTime/hours) + ':' + Math.floor((rtn.ytTime % hours)/60) + ':' + rtn.ytTime % 60
  rtn.defaultUrl = sargs.shortcutParameter.replace(/(?:\d+:)?(?:\d+:)?(?:\d+)?$/,rtn.podTimeStr)
  rtn.podUrl = rtn.defaultUrl
}
log(rtn)

FinishScript()
