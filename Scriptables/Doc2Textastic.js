// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: orange; icon-glyph: magic;
const typeK = "!type"
const cmplSetK = "completionSets"
function processObj(name, obj) {
  let rtn = {
    "contexts" : {
      "description": "Native objects, " + name,
      "scope": "source.js - comment - string",
      "pattern": `\\b${name}\\.(\\w*)`,
      "completionCaptureIndex": 1,
      "completionSetNames": [
        "scriptable.native-objects." + name
    ]},
    "completionSets" : {  
      "name": "scriptable.native-objects."+name,  
      "strings": [],  
    }
  }
  for(prop in obj){
    if(!prop.startsWith("!")){
      info = obj[prop]
      if(info.hasOwnProperty(typeK)){
        typeStr = info[typeK]
        fnRe = /fn\((.*)\)/ 
        if(!fnRe.test(typeStr)){
          logWarning(typeStr)
          continue
        }
        repl = typeStr.replace(fnRe, "($1)")  
        paramRe =/\b(\w*)\: ([?\w*]?)/g
        var idx = 1
        function paramRpl(match, name, type, offset, input){
          return `\${${idx++}:${name}}`
        }
        repl = repl.replace(paramRe, paramRpl)
        returnRe =/ \-\>(.*)/
        repl = repl.replace(returnRe, "$${0://$1}")
        rtn.completionSets.strings.push(
        {'string':prop,"append":repl,"orig":typeStr})
      }
    }
  }
  return rtn
}

let icfm = FileManager.iCloud()

let fpath = icfm.documentsDirectory() + "/scriptable.json"
let jpath = icfm.documentsDirectory() + "/jsTmpl.json"

if(!icfm.fileExists(fpath) || !icfm.fileExists(jpath)) {
  console.warn(`File "${fpath}" or "${jpath}" doesn't exist.` )
  Script.complete()
}

let jdata = JSON.parse(icfm.readString(fpath))

let txtFunctions = []
let txtObjInfo = {}
for(attr in jdata) {
  if(jdata[attr].hasOwnProperty("!type")) {
//     log(jdata[attr])
    txtFunctions.push({"string":attr, "append":`(\${1:${jdata[attr]["!type"]}})`})
  } else if (!attr.startsWith("!")) {
//     logWarning(`${attr} = ${JSON.stringify(jdata[attr], null, "  ")}`)  
    txtObjInfo[attr] = processObj(attr, jdata[attr])
  } else {
//     logError(`${attr} = ${JSON.stringify(jdata[attr], 2)}`)
  }
}

let scrGlobal = {"name": "scriptable.global.functions", "strings": txtFunctions}

let template = icfm.readString(jpath)
let tmplJson = JSON.parse(template.replace("//INSERT HERE", ""))
for (objName in txtObjInfo) {
  let obj = txtObjInfo[objName]
  for (section in obj) {
    tmplJson[section].push(obj[section])
  }
}
let txtJsonText= JSON.stringify(tmplJson, null, 2)
let outpath = icfm.documentsDirectory() + "/js.mod.json"
icfm.writeString(outpath, txtJsonText)


let pieces = template.split("//INSERT HERE")

let txtFullTxt = `${pieces[0]}${JSON.stringify(scrGlobal,2)},${pieces[1]}`
// icfm.writeString(outpath, txtFullTxt)

throw new Error("message")
let txtcb = new CallbackURL("textastic://x-callback-url/replace")
txtcb.addParameter("location", "local")
txtcb.addParameter("path", "#Textastic/CodeCompletion")
txtcb.addParameter("name", "js.json")
txtcb.addParameter("text", txtJsonText)
let res = await txtcb.open()

log(res)
res = await new CallbackURL("textastic://x-callback-url/reloadCustomizations").open()
log(res)

Script.setShortcutOutput(txtFullTxt)
Script.complete()
