// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: purple; icon-glyph: magic; share-sheet-inputs: plain-text, file-url;
let txtFile = ""
if (config.runsInApp) {
  txtFile = "<>1 d65a50c909bc9931<\><bob>2 d75a50c909bc9931<\bob>"
}
else {
  txtFile = args.plainTexts[0]
}

log(txtFile)

let rex = /(\d) (\w{16})/mg
let rexD = /(\d)/g

// QuickLook.present(txtFile)

let result = []
// log(rex.exec(txtFile))
for (m of txtFile.matchAll(rex)) {
  result.push(m[1] + " - " + m[2].match(rexD).slice(0, 9).join(""))
}

let alt = new Alert()
alt.title = "Results"
alt.message = result.join("\n")
alt.addAction("Okay")
await alt.present()
