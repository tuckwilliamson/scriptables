// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: deep-purple; icon-glyph: magic;
// share-sheet-inputs: plain-text;
let defCal = await Calendar.defaultForReminders()

let compList = await Reminder.completedToday([defCal])


let todoList = await Reminder.allIncomplete([defCal])

rtn = {complete:compList, todo:todoList}

rtn = JSON.stringify(rtn, undefined, 2)
//if (config.runsInApp){
  log(rtn)
  // Pasteboard.copyString(rtn)
  //return rtn
//}
Script.setShortcutOutput(rtn)
Script.complete()
return rtn
