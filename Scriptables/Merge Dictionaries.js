// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: deep-blue; icon-glyph: magic;
// share-sheet-inputs: plain-text;
// This script simply allows you to see what imputs were passed to the script. 
log(args.plainTexts)

let ret = {}
let params = [ret]
let info = []

for(item of args.plainTexts) {
  try {
    
    let json = JSON.parse(item)
    if (typeof(json) === typeof(ret)) {
      params.push(json)
    }
    info.push([item, json,typeof(json)])
    
  }
  catch(err) {
    // Don't care about errors using this to filter.
    
  }
}

if (! params.length) {
  throw("No dictionaries to merge!\n"+
    JSON.stringify([args.plainTexts, info], 2))
}
if (params.length === 1) {
  return params[0]
}

// params.unshift(ret)

Object.assign.apply(null,params)

// await QuickLook.present(ret)
// ret['info']=info
Script.setShortcutOutput(ret)
Script.complete()
return ret
