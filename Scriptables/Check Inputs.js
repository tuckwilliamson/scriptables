// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: deep-blue; icon-glyph: magic;
// share-sheet-inputs: plain-text, image, url, file-url;
// This script simply allows you to see what imputs were passed to the script. 


logWarning(URLScheme.forRunningScript())
log(args)
lcl = FileManager.local()
let inputs = {"args.fileURLs":args.fileURLs,
"args.images": args.images,
"args.notification": args.notification,
"args.plainTexts": args.plainTexts,
"args.queryParameters": args.queryParameters,
"args.shortcutParameter": args.shortcutParameter,
"args.urls": args.urls,
 "fileUrlContent":args.fileURLs.map(f=>lcl.readString(f))
}

for(k in config){
  inputs[k] = config[k]
}

let out = JSON.stringify(inputs, 2)

await QuickLook.present(out)
log(out)
Script.setShortcutOutput(out)
Script.complete()


