// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: deep-purple; icon-glyph: magic;
var mdFromRmdr = function(rmdr){
  let enc = ""
  if(rmdr.priority){
      enc = toString(rmdr.priority)
  }
  let chk = rmdr.isCompleted ? "X":" "
  return `- [${chk}] ${enc}${rmdr.title}${enc}\n`
}

let defCal = await Calendar.defaultForReminders()

let compList = await Reminder.completedToday([defCal])

let rtn = "#### To Do for [[date|%a %b %e, %Y at %H:%M]]\n____\n##### Tuck's List\n"

if(compList.length){
  rtn += "###### Completed\n_____\n"
  
  compList.forEach((rmdr) => {
    rtn += mdFromRmdr(rmdr)})
}

let todoList = await Reminder.allIncomplete([defCal])

if(todoList.length){
  rtn += "###### To Do:\n_____\n"
  
  todoList.forEach((rmdr) => {
    rtn += mdFromRmdr(rmdr)})
}

// log(rtn)



let draft = new CallbackURL("drafts4://x-callback-url/runAction?")
draft.addParameter("action", "Test")
draft.addParameter("text", rtn)

if (config.runsInApp){
  let htm = await draft.open()

  rtn = Pasteboard.pasteString()
  log(rtn)
  Pasteboard.copyString(rtn)
  return rtn
}
rtn = draft.getURL()

Script.setShortcutOutput(rtn)
return rtn
Script.complete()
